package com.example.petclinic.services.jpa;

import com.example.petclinic.entity.Owner;
import com.example.petclinic.entity.Pet;
import com.example.petclinic.repository.OwnerRepository;
import com.example.petclinic.repository.PetRepository;
import com.example.petclinic.services.OwnerService;
import com.example.petclinic.services.PetService;
import com.example.petclinic.services.config.JpaImplementation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Transactional(readOnly = true)
@JpaImplementation
public class OwnerJpaService extends AbstractJpaService<Owner,Long> implements OwnerService {

    private final OwnerRepository ownerRepository;
    private final PetService petService;
    private final PetRepository petRepository;

    @Override
    public JpaRepository<Owner, Long> getRepository() {
        return ownerRepository;
    }

    @Override
    public Collection<Owner> findByName(String name) {
        throw new UnsupportedOperationException();
    }

    public void readUserAndPet() {
        List<Owner> owners = ownerRepository.findAll();
        for (Owner owner : owners) {
            log.warn("Owner name {}", owner.getFullName());
            for (Pet pet : owner.getPets()) {
                log.warn("Pet name: {}", pet.getName());
            }
        }
    }

    @Transactional(isolation = Isolation.READ_UNCOMMITTED)
    public void updateOwnerAndPet(Long id, String lastName, String petName) {
        Owner owner = ownerRepository.findById(id).orElseThrow();
        owner.setLastName(lastName);

        if (CollectionUtils.isNotEmpty(owner.getPets())) {
            log.info("Previous pet name: {}", owner.getPets().get(0).getName());
            updatePetName(owner.getPets().get(0).getId(), petName);
        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void updatePetName(Long petId, String petName) {
        try {
            Pet pet = petRepository.findById(petId).orElseThrow();
            pet.setName(petName);
        } catch (Exception ex) {
            throw new RuntimeException();
        }
    }
}
