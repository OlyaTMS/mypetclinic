package com.example.petclinic.services;

import com.example.petclinic.entity.BaseEntity;

import java.util.Map;

public interface BaseMapService<T, ID> {
    Map<ID, T> getResource();
}
