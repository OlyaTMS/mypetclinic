package com.example.petclinic.controller;

import com.example.petclinic.services.VetService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
@RequiredArgsConstructor
@Controller
public class VetController {

    private final VetService vetService;
}
