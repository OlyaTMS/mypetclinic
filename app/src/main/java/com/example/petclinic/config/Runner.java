package com.example.petclinic.config;

import com.example.petclinic.entity.*;
import com.example.petclinic.repository.*;
import jdk.jfr.Category;
import lombok.RequiredArgsConstructor;
import org.jasypt.encryption.StringEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;

@RequiredArgsConstructor
@Component("mainRunner")
public class Runner implements CommandLineRunner {
    @Value("${spring.datasource.password:undefined}")
    private String dbPwd;

    @Qualifier("customEncryptor")
    @Autowired
    StringEncryptor stringEncryptor;

    private final VisitRepository visitRepository;
    private final NotesRepository notesRepository;
    private final ProductRepository productRepository;
    private final ItemRepository itemRepository;
    private final ReceiptRepository receiptRepository;
    private final PermissionRepository permissionRepository;
    private final UserRepository userRepository;
    private final UOMRepository uomRepository;
    private final OwnerRepository ownerRepository;
    private final CatRepository catRepository;
    private final DogRepository dogRepository;
    private final PetRepository petRepository;
    private final VipClientRepository vipClientRepository;
    private final VetRepository vetRepository;

    @Transactional
    @Override
    public void run(String... args) throws Exception {

//        createPets();
//
//        createOwner();
//
//        createVisit();
//
//        createReceipt();

//        initData();
//
//        encryption();
//
//        //createProduct();
//
//        createUser();
//
//        createVipClient();
        
//        createVet();
        

    }

    private void createVet() {
        Dentist dentist = Dentist.builder()
                .firstName("Ivan")
                .lastName("Ivanov")
                .specification("dentist 1 category")
                .dentistDescription("dentist description")
                .build();

        Surgeon surgeon = Surgeon.builder()
                .firstName("Petr")
                .lastName("Petrov")
                .specification("surgeon 2 category")
                .surgeonDescription("surgeon description")
                .build();

        vetRepository.saveAll(Arrays.asList(surgeon, dentist));

    }

    private void createVipClient() {
        VipClient vipClient = VipClient.builder()
                .firstName("Alex")
                .lastName("Grouk")
  //              .phone("+375-44-111-22-33")
                .vipCard("vip1234")
                .loyaltyAmount(120L)
                .loyaltyProgram("program_1")
                .discount(10L)
                .build();

        vipClientRepository.save(vipClient);
    }

    private void createPets() {
        Cat cat = Cat.builder()
                .birthDate(LocalDate.now())
                .catName("Barsic")
                .name("Barsic")
                .build();
        catRepository.save(cat);

        Dog dog = Dog.builder()
                .birthDate(LocalDate.now())
                .dogName("Reks")
                .name("Reks")
                .build();

        dogRepository.save(dog);

        Pet pet = Pet.builder()
                .birthDate(LocalDate.now())
                .name("undefined")
                .build();

        petRepository.save(pet);

        System.out.println("Pets in db " + petRepository.count());
    }

    private void createOwner() {

        List<Pet> pets = petRepository.findAll();

        Owner owner = Owner.builder()
                .firstName("Olya")
                .lastName("Khamitskaya")
                .pets(pets)
                .contactDetails(ContactDetails.builder()
                        .phone("+375-44-708-17-85")
                        .build())
                .build();
        for (Pet pet : pets) {
            pet.setOwner(owner);
        }

        ownerRepository.save(owner);
        petRepository.saveAll(pets);
    }

    @Transactional
    public User getUser() {
        User user = userRepository.findByLogin("user1").orElseThrow();
        System.out.println(user.getLogin());
        return user;
    }

    private void createUser() {
        Permission perm = permissionRepository.findByName("update_user")
                .orElseThrow(NoSuchElementException::new);

        Filial filial = Filial.builder()
                .filialName("filial1")
                .city("Minsk")
                .build();

        User user1 = User.builder()
                .login("user1")
                .password("1234")
                .role(Role.ADMIN)
                .permissions(Collections.singleton(perm))
                .filials(Collections.singleton(filial))
                .build();

        User user2 = User.builder()
                .login("user2")
                .password("2222")
                .role(Role.MANAGER)
                .permissions(Collections.singleton(perm))
                .filials(Collections.singleton(filial))
                .build();
        userRepository.saveAll(Arrays.asList(user1, user2));
}

//    private void initData() {
//        Permission permission1 = Permission.builder()
//                .name("update_user")
//                .build();
//        Permission permission2 = Permission.builder()
//                .name("delete_user")
//                .build();
//        Permission permission3 = Permission.builder()
//                .name("create_visit")
//                .build();
//        Permission permission4 = Permission.builder()
//                .name("get_pet")
//                .build();
//        permissionRepository.saveAll(Arrays.asList(permission1, permission2, permission3, permission4));
//
//        UnitOfMeasure unitOfMeasure1 = UnitOfMeasure.builder()
//                .code(UOM.KG)
//                .codeIso("KGR")
//                .description("kilo")
//                .build();
//
//        UnitOfMeasure unitOfMeasure2 = UnitOfMeasure.builder()
//                .code(UOM.PACK)
//                .codeIso("PACK")
//                .description("pack")
//                .build();
//
//        UnitOfMeasure unitOfMeasure3 = UnitOfMeasure.builder()
//                .code(UOM.BOX)
//                .codeIso("BOX_BIG")
//                .description("box big XL")
//                .build();
//
//        uomRepository.saveAll(Arrays.asList(unitOfMeasure1, unitOfMeasure2, unitOfMeasure3));
//    }

    private void createReceipt() {

        List<Visit> visits = visitRepository.findAll();
        Receipt receipt1 = Receipt.builder()
                .amount(BigDecimal.TEN)
                .visit(visits.get(0))
                .build();
        Item item1 = Item.builder()
                .name("coffee")
                .price(BigDecimal.TEN)
                .receipt(receipt1)
                .build();
        Item item2 = Item.builder()
                .name("pizza")
                .price(BigDecimal.TEN)
                .receipt(receipt1)
                .build();

        receipt1.setItems(Arrays.asList(item1, item2));
        visits.get(0).setReceipt(receipt1);
        receiptRepository.save(receipt1);
        System.out.println("Amount in usd = " + receipt1.getAmountInUsd());

        Receipt receipt2 = Receipt.builder()
                .amount(BigDecimal.TEN)
                .visit(visits.get(1))
                .build();

        visits.get(1).setReceipt(receipt2);
        receiptRepository.save(receipt2);
        visitRepository.saveAll(visits);

    }

    private void createProduct() {
        UnitOfMeasure uom = uomRepository.findById(UOM.PACK).orElseThrow();

        Product product1 = Product.builder()
                .name("Cola")
                .price(BigDecimal.TEN)
                .productId(ProductPK.builder()
                        .code("001")
                        .codePart("abc")
                        .build())
                .unitOfMeasure(uom)
                .build();
        Product product2 = Product.builder()
                .name("Fanta")
                .price(BigDecimal.TEN)
                .productId(ProductPK.builder()
                        .code("002")
                        .codePart("abc")
                        .build())
                .unitOfMeasure(uom)
                .build();

        productRepository.save(product1);
        productRepository.save(product2);
    }

    private void createVisit() {

        Pet pet = petRepository.findAll().get(0);

        Notes notes1 = Notes.builder().description("desc1").build();
        Notes notes2 = Notes.builder().description("desc2").build();

        Visit visit1 = Visit.builder().time(OffsetDateTime.now())
                .version(1)
                .notes(notes1)
                .pet(pet)
                .build();
        Visit visit2 = Visit.builder().time(OffsetDateTime.now())
                .version(1)
                .notes(notes2)
                .pet(pet)
                .build();
        visitRepository.saveAll(Arrays.asList(visit1, visit2));

    }

    private void encryption() {
//        String pwd = "prod_pwd";
//        String encrypt = stringEncryptor.encrypt(pwd);
//        System.out.println(encrypt);
//
//        String decrypt = stringEncryptor.decrypt(encrypt);
//        System.out.println(decrypt);
        System.out.println(dbPwd);
    }
}
