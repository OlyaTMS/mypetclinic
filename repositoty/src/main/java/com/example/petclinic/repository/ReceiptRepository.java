package com.example.petclinic.repository;

import com.example.petclinic.entity.Receipt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;


public interface ReceiptRepository extends JpaRepository<Receipt,Long> {

    @Transactional
    @Modifying
    @Query("DELETE from Receipt r where r.status = com.example.petclinic.entity.ReceiptStatus.CANCELLED")
    void deleteCancelled();

}
