package com.example.petclinic.repository;

import com.example.petclinic.entity.Cat;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CatRepository extends JpaRepository<Cat,Long> {
}
