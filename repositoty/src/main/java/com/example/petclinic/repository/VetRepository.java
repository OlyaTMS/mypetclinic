package com.example.petclinic.repository;

import com.example.petclinic.entity.Vet;

public interface VetRepository extends PersonRepository<Vet,Long> {
}
