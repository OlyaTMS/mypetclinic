package com.example.petclinic.repository;

import com.example.petclinic.entity.Product;
import com.example.petclinic.entity.ProductPK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, ProductPK> {
}
