package com.example.petclinic.repository;

import com.example.petclinic.entity.UOM;
import com.example.petclinic.entity.UnitOfMeasure;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UOMRepository extends JpaRepository<UnitOfMeasure, UOM> {
}
