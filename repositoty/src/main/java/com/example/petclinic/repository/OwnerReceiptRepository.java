package com.example.petclinic.repository;

import com.example.petclinic.entity.OwnerReceipt;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OwnerReceiptRepository extends JpaRepository<OwnerReceipt,Long> {

    List<OwnerReceipt> findByPayerNameContainsIgnoreCase(String name);
}
