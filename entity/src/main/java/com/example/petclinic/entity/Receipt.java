package com.example.petclinic.entity;

import lombok.*;
import org.apache.commons.collections4.CollectionUtils;


import javax.persistence.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@Entity
public class Receipt {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private BigDecimal amount;
    @Transient
    private BigDecimal amountInUsd;

    @OneToMany(mappedBy = "receipt", cascade = CascadeType.ALL)
    private List<Item> items;

    @OneToOne(mappedBy = "receipt")
    private Visit visit;

    @Builder.Default
    @Enumerated(EnumType.STRING)
    private ReceiptStatus status = ReceiptStatus.PENDING;

    @PrePersist
    @PreUpdate
    private void calculateAmount() {
        BigDecimal amount = BigDecimal.ZERO;
        if (CollectionUtils.isNotEmpty(items)) {
            for (Item item : items) {
                amount = amount.add(item.getPrice());
            }
        }
        this.amount = amount;
    }

    @PostLoad
    @PostPersist
    @PostUpdate
    private void calculateAmountInUsd() {
        this.amountInUsd = amount.divide(BigDecimal.valueOf(2.5), RoundingMode.HALF_UP);
    }
}
