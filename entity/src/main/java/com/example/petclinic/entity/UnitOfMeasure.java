package com.example.petclinic.entity;

import lombok.*;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
@Entity
@Table(name = "uom")
public class UnitOfMeasure {

    @Id
    @Enumerated(EnumType.STRING)
    private UOM code;
    private String codeIso;
    private String description;
}
