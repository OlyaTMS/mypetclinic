package com.example.petclinic.entity;

import lombok.*;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.math.BigDecimal;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@Entity
public class Product {

    @EmbeddedId
    private ProductPK productId;
    private String name;
    private BigDecimal price;

    @ManyToOne
    private UnitOfMeasure unitOfMeasure;
}
